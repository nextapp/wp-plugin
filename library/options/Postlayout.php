<?php
class Nextapp_Options_Postlayout extends Nextapp_Options
{
	public function getType()
	{
		return self::ELEMENT_SELECT;
	}
	
	public function getName()
	{
		return 'nextapp_postlayout';
	}
	
	public function getTitle()
	{
		return '文章布局';
	}
	
	public function getDefault()
	{
		return current(array_keys($this->getOptions()));
	}
	
	public function getOptions()
	{
		return array(
			'1'	=> '纯文字类型',
			'2'	=> '小图片类型',
			'3'	=> '大图片类型'
		);
	}
}